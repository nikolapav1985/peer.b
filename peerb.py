#!/usr/bin/python

def new_line():
    """
        print a single line containing a dot
    """
    print('.')
def three_lines():
    """
        print three lines
        each line contains a dot
    """
    new_line()
    new_line()
    new_line()
def nine_lines():
    """
        print nine lines
        each line contains a dot
    """
    three_lines()
    three_lines()
    three_lines()
def clear_screen():
    """
        print 25 lines
        each line contains a dot
    """
    nine_lines()
    nine_lines()
    three_lines()
    three_lines()
    new_line()

if __name__ == "__main__":
    print('Printing nine lines')
    nine_lines()
    print('Going to clear screen')
    clear_screen()

"""
OUTPUT

Printing nine lines
.
.
.
.
.
.
.
.
.
Going to clear screen
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.
.

"""
