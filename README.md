Programming assignment unit 2
-----------------------------

A program for programming assignment unit 2. Program prints 9 lines and after 25 lines in order to clear screen.

Included files
--------------

peerb.py (implementation of print and main functions, example output given in comments).

Test environment
----------------

OS lubuntu 16.04 lts kernel version 4.13.0. Python version 2.7.12.

Example test
------------

Type ./peerb.py (or any other file).
